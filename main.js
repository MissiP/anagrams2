let anagramWords = {}
let noOfWords = 5;

words.forEach (word => {
    let alphaWordOne = alphabetize(word)

    if (!anagramWords[alphaWordOne]) {
        anagramWords[alphaWordOne] = [word]
    }
    else {
        anagramWords[alphaWordOne].push(word)
    }
  
})
console.log(anagramWords)
for (let key in anagramWords) {

    if (anagramWords[key].length==noOfWords){
           
        display("'"+ key +"' ---> "+ anagramWords[key], "div");
    }
}

function alphabetize(a) {  
    return  a.toLowerCase().split("").sort().join("").trim();
}

function display (value, element) {
    
    var destination = document.getElementById("d1");
    // creates div element and adds value to it 
    var newElement = document.createElement(element);
    var newText = document.createTextNode(value);
    newElement.appendChild(newText);
    // adds div element to d1 div
    destination.appendChild(newElement);
}